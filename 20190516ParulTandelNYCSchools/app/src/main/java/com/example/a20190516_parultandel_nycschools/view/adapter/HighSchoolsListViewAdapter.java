package com.example.a20190516_parultandel_nycschools.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a20190516_parultandel_nycschools.R;
import com.example.a20190516_parultandel_nycschools.databinding.HighschoolDetailsBinding;
import com.example.a20190516_parultandel_nycschools.model.HighSchoolDetails;
import com.example.a20190516_parultandel_nycschools.view.activity.TestScoreDisplayActivity;
import com.example.a20190516_parultandel_nycschools.viewmodel.HighSchoolDetailsViewModel;
import com.example.a20190516_parultandel_nycschools.viewmodel.TestScoreViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Recycler view adapter :binding viewmodel with adapter
 */
public class HighSchoolsListViewAdapter extends RecyclerView.Adapter<HighSchoolsListViewAdapter.HghSchoolAdapterViewHolder> {
    private List<HighSchoolDetails> highSchoolList;

    public HighSchoolsListViewAdapter() {this.highSchoolList = Collections.emptyList();}

    @NonNull
    @Override
    public HghSchoolAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        HighschoolDetailsBinding highschoolDetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.highschool_details ,viewGroup, false);
        return new HghSchoolAdapterViewHolder(highschoolDetailsBinding);

    }

    /**
     * Bind view with adapter for each position
     * Set the tag of view with school's dbn
     * @param hghSchoolAdapterViewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull HghSchoolAdapterViewHolder hghSchoolAdapterViewHolder, int position) {
        hghSchoolAdapterViewHolder.bindHichSchoolDetails(highSchoolList.get(position));
        hghSchoolAdapterViewHolder.itemView.setTag(highSchoolList.get(position).dbn);
    }

    @Override
    public int getItemCount() {
        return  highSchoolList.size();
    }

    public void setHighSchoolsList(List<HighSchoolDetails> highSchoolList) {
        this.highSchoolList = highSchoolList;
        notifyDataSetChanged();
    }

    /**
     * Binding schools details with view holder
     */
    public class HghSchoolAdapterViewHolder extends RecyclerView.ViewHolder {

        HighschoolDetailsBinding highschoolDetailsBinding;

        public HghSchoolAdapterViewHolder(HighschoolDetailsBinding highschoolDetailsBinding) {
                super(highschoolDetailsBinding.schoolDetails);
                this.highschoolDetailsBinding=highschoolDetailsBinding;
        }
        void bindHichSchoolDetails(HighSchoolDetails highSchoolDetails){
            if(highschoolDetailsBinding.getSchoolDetailsViewModel() == null){
                highschoolDetailsBinding.setSchoolDetailsViewModel(new HighSchoolDetailsViewModel(highSchoolDetails, itemView.getContext()));
                highschoolDetailsBinding.setScoreViewModel(new TestScoreViewModel(itemView.getContext()));
            }else {
                highschoolDetailsBinding.getSchoolDetailsViewModel().setHighSchoolDetails(highSchoolDetails);
            }
        }
    }
}
