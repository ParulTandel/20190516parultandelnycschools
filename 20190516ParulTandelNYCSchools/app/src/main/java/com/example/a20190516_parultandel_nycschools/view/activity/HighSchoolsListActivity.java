package com.example.a20190516_parultandel_nycschools.view.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.example.a20190516_parultandel_nycschools.R;
import com.example.a20190516_parultandel_nycschools.databinding.HighSchoolsListBinding;
import com.example.a20190516_parultandel_nycschools.databinding.HighschoolDetailsBinding;
import com.example.a20190516_parultandel_nycschools.view.adapter.HighSchoolsListViewAdapter;
import com.example.a20190516_parultandel_nycschools.viewmodel.HighSchoolViewModel;

import java.util.Observable;
import java.util.Observer;

/**
 * Display list of High Schools
 */
public class HighSchoolsListActivity extends AppCompatActivity implements Observer {

    private HighSchoolsListBinding highSchoolsListBinding;
    private HighSchoolViewModel highSchoolViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();
        setSupportActionBar(highSchoolsListBinding.toolbar);
        setUpListOfUsersView(highSchoolsListBinding.listHighSchools);
        setUpObserver(highSchoolViewModel);
    }

    /**
     * Binding between view and viewmodel
     */
    private void initDataBinding() {
        highSchoolsListBinding = DataBindingUtil.setContentView(this, R.layout.high_schools_list);
        highSchoolViewModel = new HighSchoolViewModel(this);
        highSchoolsListBinding.setSchoolViewModel(highSchoolViewModel);



    }

    /**
     * set up the list of highschools with recycler view
      */

    private void setUpListOfUsersView(RecyclerView listUser) {
        HighSchoolsListViewAdapter viewAdapter = new HighSchoolsListViewAdapter();
        listUser.setAdapter(viewAdapter);
        listUser.setLayoutManager(new LinearLayoutManager(this));
    }
    public void setUpObserver(Observable observable) {
        observable.addObserver(this);
    }

    /**
     * update the adapter for the list with updated data
     * @param observable
     * @param o
     */
    @Override
    public void update(Observable observable, Object o) {

        if(observable instanceof  HighSchoolViewModel) {
            HighSchoolsListViewAdapter viewAdapter = (HighSchoolsListViewAdapter) highSchoolsListBinding.listHighSchools.getAdapter();
            HighSchoolViewModel highSchoolViewModel = (HighSchoolViewModel) observable;
            viewAdapter.setHighSchoolsList(highSchoolViewModel.getHighSchoolsList());
        }
    }
    @Override protected void onDestroy() {
        super.onDestroy();
        highSchoolViewModel.reset();
    }

}
