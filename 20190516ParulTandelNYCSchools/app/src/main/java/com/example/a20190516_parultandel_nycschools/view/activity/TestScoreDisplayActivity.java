package com.example.a20190516_parultandel_nycschools.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.example.a20190516_parultandel_nycschools.R;
import com.example.a20190516_parultandel_nycschools.databinding.TestScoreDisplayBinding;
import com.example.a20190516_parultandel_nycschools.model.TestScoreDetails;
import com.example.a20190516_parultandel_nycschools.viewmodel.TestScoreDetailsViewModel;

/**
 * Display  test score of particular high school
 */
public class TestScoreDisplayActivity extends AppCompatActivity  {


    private TestScoreDisplayBinding testScoreDisplayBinding;
    private static final String TEST_SCORE = "TEST_SCORE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        testScoreDisplayBinding = DataBindingUtil.setContentView(this, R.layout.test_score_display);
        setSupportActionBar(testScoreDisplayBinding.toolbar);
        displayHomeAsUpEnabled();
        getExtrasFromIntent();
    }

    private void displayHomeAsUpEnabled() {

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static Intent fillDetail(Context context, TestScoreDetails testScoreDetails) {
        Intent intent = new Intent(context, TestScoreDisplayActivity.class);
        intent.putExtra(TEST_SCORE, testScoreDetails);
        return intent;
    }

    private void getExtrasFromIntent(){
        TestScoreDetails testScoreDetails = (TestScoreDetails) getIntent().getSerializableExtra(TEST_SCORE);
        TestScoreDetailsViewModel testScoreDetailsViewModel = new TestScoreDetailsViewModel(testScoreDetails);

        //view bingidng with view model
        testScoreDisplayBinding.setTestScoreDetailViewModel(testScoreDetailsViewModel);

    }

}
