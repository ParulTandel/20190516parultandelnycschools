package com.example.a20190516_parultandel_nycschools.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Model for test score details of particular high school
 */
public class TestScoreDetails implements Serializable {
    @SerializedName("school_name")public String schoolName;
    @SerializedName("num_of_sat_test_takers")public String noOfTestTakers;
    @SerializedName("sat_critical_reading_avg_score")public String readingAvgScore;
    @SerializedName("sat_math_avg_score")public String mathAvgScore;
    @SerializedName("sat_writing_avg_score")public String writingAvgScore;
}
