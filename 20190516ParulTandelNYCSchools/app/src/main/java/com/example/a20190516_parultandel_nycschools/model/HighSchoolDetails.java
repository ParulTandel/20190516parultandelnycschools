package com.example.a20190516_parultandel_nycschools.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Model for HighSchool details
 */

public class HighSchoolDetails implements Serializable {

    @SerializedName("school_name")public String schoolName;
    @SerializedName("website")public String webSite;
    @SerializedName("dbn")public String dbn;
    @SerializedName("primary_address_line_1")public String primaryAddress;
    @SerializedName("zip")public String zipCode;
    @SerializedName("city")public String city;
    @SerializedName("phone_number")public String phoneNumber;
    public String schoolAddress;
}
