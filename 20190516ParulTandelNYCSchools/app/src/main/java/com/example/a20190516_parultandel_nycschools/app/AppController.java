package com.example.a20190516_parultandel_nycschools.app;

import android.app.Application;
import android.content.Context;

import com.example.a20190516_parultandel_nycschools.network.ApiFactory;
import com.example.a20190516_parultandel_nycschools.network.HighSchoolService;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 *This calss create the request to call api
 */

public class AppController extends Application {

    private HighSchoolService highSchoolService;
    private Scheduler scheduler;

    private static AppController get(Context context) {
        return (AppController) context.getApplicationContext();
    }

    public static AppController create(Context context) {
        return AppController.get(context);
    }

    /**
     * method creats retrofit client
     * @return
     */
    public HighSchoolService getSchools() {
        if (highSchoolService == null) {
            highSchoolService = ApiFactory.create();
        }

        return highSchoolService;
    }

    /**
     * method to call for network request
     * @return
     */
    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }

        return scheduler;
    }

}
