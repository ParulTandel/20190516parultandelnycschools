package com.example.a20190516_parultandel_nycschools.viewmodel;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.example.a20190516_parultandel_nycschools.app.AppController;
import com.example.a20190516_parultandel_nycschools.model.TestScoreDetails;
import com.example.a20190516_parultandel_nycschools.network.HighSchoolService;
import com.example.a20190516_parultandel_nycschools.view.activity.TestScoreDisplayActivity;

import java.util.List;
import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;


/**
 * ViewModel: call retrofit service and fetch data for model
 * Fetching test score data of highschool and notify view
 */
public class TestScoreViewModel extends Observable {

    public String dbn;
    public Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private static final String TAG="TestScoreViewModel";

    public TestScoreViewModel(Context context){
        this.context=context;

    }

    /**
     * function will be called on the click event of list item
     * @param v
     */
    public void onItemClick(View v){
        //TODO : call the view to diplay highschool's test scores  on selection of each school from the list
        String clickedItem=(String) v.getTag();
        dbn=clickedItem;
        stratACtivity();
    }

    /**
     * call service to get the score details of high school with quesry parameter dbn
     */
    private void stratACtivity() {
        AppController appController = AppController.create(context);
        HighSchoolService highSchoolService = appController.getSchools();

        Disposable disposable = highSchoolService.getTestScore(dbn)
                .subscribeOn(appController.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<TestScoreDetails>> (){
                    @Override
                    public void accept(List<TestScoreDetails> testScoreDetailsList) throws Exception {

                        // if success start TestscoreDisplay activity with test score data
                        if(testScoreDetailsList.isEmpty())
                            Toast.makeText(context, "No TestScore data available for this Highschool", Toast.LENGTH_SHORT).show();

                        else
                            context.startActivity(TestScoreDisplayActivity.fillDetail(context, testScoreDetailsList.get(0)));
                    }

                }, new Consumer<Throwable>() {

                    //if service call fails
                    @Override public void accept(Throwable throwable) throws Exception {
                        Log.e(TAG,throwable.getMessage());
                    }
                });

        compositeDisposable.add(disposable);
    }
}
