package com.example.a20190516_parultandel_nycschools.network;

import io.reactivex.Observable;;

import com.example.a20190516_parultandel_nycschools.model.HighSchoolDetails;
import com.example.a20190516_parultandel_nycschools.model.TestScoreDetails;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Retrofit interface to call services get highschools and score detials of highschool with Query parameter "dbn"
 */
public interface HighSchoolService {
    @GET("resource/s3k6-pzi2.json")
    Observable<List<HighSchoolDetails>> fetchHighSchools();

    @GET("resource/f9bf-2cp4.json?")
    Observable<List<TestScoreDetails>> getTestScore(@Query("dbn") String dbn);
}
