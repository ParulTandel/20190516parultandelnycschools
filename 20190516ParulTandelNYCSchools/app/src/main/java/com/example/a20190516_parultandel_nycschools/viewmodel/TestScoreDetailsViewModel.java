package com.example.a20190516_parultandel_nycschools.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;

import com.example.a20190516_parultandel_nycschools.model.TestScoreDetails;

/**
 * ViewModel:insert test score data to UI
 */

public class TestScoreDetailsViewModel extends BaseObservable {

    TestScoreDetails testScoreDetails;

    /**
     * constructor
     * @param testScoreDetails
     */
    public TestScoreDetailsViewModel(TestScoreDetails testScoreDetails){
        this.testScoreDetails=testScoreDetails;

    }

    /**
     * get number of test takers
     * @return
     */
    public String getNoOfTestTakers(){
        if(testScoreDetails.noOfTestTakers != null)
            return testScoreDetails.noOfTestTakers;
        else
            return "None";
    }

    /**
     * get high school name
     * @return
     */
    public String getName()
    {
        if(testScoreDetails.schoolName != null)
            return testScoreDetails.schoolName;
        else
            return"None";
    }

    /**
     * get reading average score
     * @return
     */
    public String getReadingAvgScore()
    {
        if(testScoreDetails.readingAvgScore!=null)
            return testScoreDetails.readingAvgScore;
        else
         return "None";
    }

    /**
     * get Math average score
     * @return
     */
    public String getMathcAvgScore()
    {
        if(testScoreDetails.mathAvgScore!=null)
            return testScoreDetails.mathAvgScore;
        else
            return "None";
    }
    public void setTestScoreDetails(TestScoreDetails testScoreDetails)
    {
        this.testScoreDetails=testScoreDetails;
        notifyChange();
    }




}
