package com.example.a20190516_parultandel_nycschools.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.a20190516_parultandel_nycschools.utils.Constants.BASE_URL;

/**
 * Retrofit client for api call
 */
public class ApiFactory {
    public static HighSchoolService create() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(HighSchoolService.class);
    }
}
