package com.example.a20190516_parultandel_nycschools.viewmodel;

import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.example.a20190516_parultandel_nycschools.R;
import com.example.a20190516_parultandel_nycschools.app.AppController;
import com.example.a20190516_parultandel_nycschools.model.HighSchoolDetails;
import com.example.a20190516_parultandel_nycschools.network.HighSchoolService;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * ViewModel: call retrofit service and fetch data for model
 * Fetching high school data and notify View
 */
public class HighSchoolViewModel extends Observable{

    public ObservableInt progressBar;
    public ObservableInt userRecycler;
    public ObservableInt userLabel;
    public ObservableField<String> messageLabel;

    private List<HighSchoolDetails> highSchoolList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private static final String TAG="HighSchoolViewModel";

    public HighSchoolViewModel(@NonNull Context context) {
        this.context = context;
        this.highSchoolList = new ArrayList<>();
        progressBar = new ObservableInt(View.GONE);
        userRecycler = new ObservableInt(View.GONE);
        userLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_message_loading_users));
    }
    public void onClickFabToLoad(View view) {
        initializeViews();
        fetchSchoolsList();
    }

    /**
     * Visibility of view binding with view model
     */
    public void initializeViews() {
        userLabel.set(View.GONE);
        userRecycler.set(View.GONE);
        progressBar.set(View.VISIBLE);
    }

    /**
     * call the service to fetch high school details
     */
    private void fetchSchoolsList() {

        AppController appController = AppController.create(context);
        HighSchoolService highSchoolService = appController.getSchools();

        Disposable disposable = highSchoolService.fetchHighSchools()
                .subscribeOn(appController.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<HighSchoolDetails>>() {
                    @Override
                    public void accept(List<HighSchoolDetails> highSchoolDetailsList) throws Exception {

                        //if success update highschool list
                        updateHighSchoolDataList(highSchoolDetailsList);
                        progressBar.set(View.GONE);
                        userLabel.set(View.GONE);
                        userRecycler.set(View.VISIBLE);
                    }

                }, new Consumer<Throwable>() {
                    @Override public void accept(Throwable throwable) throws Exception {

                        //if failed to fet data
                        messageLabel.set(context.getString(R.string.error_message_loading_users));
                        Log.e(TAG,throwable.getMessage());
                        progressBar.set(View.GONE);
                        userLabel.set(View.VISIBLE);
                        userRecycler.set(View.GONE);
                    }
                });

        compositeDisposable.add(disposable);
    }

    /**
     * update the school list of observer:Highschool list activity
     * @param highSchools
     */
    private void updateHighSchoolDataList(List<HighSchoolDetails> highSchools) {
        if(highSchools.size()>0) {
            highSchoolList.addAll(highSchools);
            setChanged();
            notifyObservers();
        }
        else{
            Log.e(TAG,"high school list null");
        }
    }

    public List<HighSchoolDetails> getHighSchoolsList() {
        if(highSchoolList!=null && !highSchoolList.isEmpty())
            return highSchoolList;
        else
            return null;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

}
