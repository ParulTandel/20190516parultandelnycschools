package com.example.a20190516_parultandel_nycschools.viewmodel;

import com.example.a20190516_parultandel_nycschools.model.HighSchoolDetails;
import com.example.a20190516_parultandel_nycschools.model.TestScoreDetails;

import android.content.Context;
import android.databinding.BaseObservable;
import android.util.Log;
import android.view.View;

/**
 * Viewmodel: insert high school data to UI
 */

public class HighSchoolDetailsViewModel extends BaseObservable {

    private HighSchoolDetails highSchoolDetails;
    private Context context;


    /**
     * Constructor of view model
     * @param highSchoolDetails
     * @param context
     */
    public HighSchoolDetailsViewModel(HighSchoolDetails highSchoolDetails, Context context){
        this.highSchoolDetails=highSchoolDetails;

        this.context=context;
    }

    /**
     * get high school address
     * @return
     */
    public String getSchoolAddress(){
        if(highSchoolDetails.primaryAddress!=null && highSchoolDetails.city !=null && highSchoolDetails.zipCode!=null) {
            highSchoolDetails.schoolAddress = highSchoolDetails.primaryAddress + "," + highSchoolDetails.city + "," + highSchoolDetails.zipCode;
            return highSchoolDetails.schoolAddress;
        }
        else
            return "None";
    }

    /**
     * get school name
     * @return
     */
    public String getSchoolName(){
        if(highSchoolDetails.schoolName != null)
            return highSchoolDetails.schoolName;
        else
            return "None";
    }

    /**
     * get phone number
     * @return
     */
    public String getPhoneNo(){
        if(highSchoolDetails.phoneNumber != null)
            return highSchoolDetails.phoneNumber;
        else
            return "None";
    }

    /**
     * get school's website
     * @return
     */
    public String getWebSite(){
        if(highSchoolDetails.webSite!=null)
            return highSchoolDetails.webSite;
        else
            return "None";
    }

    public void setHighSchoolDetails(HighSchoolDetails highSchoolDetails)
    {
        this.highSchoolDetails=highSchoolDetails;
        notifyChange();
    }
}
