package com.example.a20190516_parultandel_nycschools;


import com.example.a20190516_parultandel_nycschools.model.HighSchoolDetails;
import com.example.a20190516_parultandel_nycschools.model.TestScoreDetails;
import com.example.a20190516_parultandel_nycschools.viewmodel.HighSchoolDetailsViewModel;
import com.example.a20190516_parultandel_nycschools.viewmodel.TestScoreDetailsViewModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertNotNull;

@RunWith(MockitoJUnitRunner.class)

public class TestScoreDetailsViewModelTest {


    private TestScoreDetailsViewModel testScoreDetailsViewModelMock;
    private TestScoreDetails testScoreDetailsMock;
    @Test
    public void setTestScoreDetailsMock(){
        testScoreDetailsMock=new TestScoreDetails();
        testScoreDetailsViewModelMock=new TestScoreDetailsViewModel(testScoreDetailsMock);
        assertNotNull(testScoreDetailsViewModelMock.getNoOfTestTakers());
        assertNotNull(testScoreDetailsViewModelMock.getReadingAvgScore());
        assertNotNull(testScoreDetailsViewModelMock.getMathcAvgScore());
        assertNotNull(testScoreDetailsViewModelMock.getName());

    }
}
