package com.example.a20190516_parultandel_nycschools;

import android.content.Context;

import com.example.a20190516_parultandel_nycschools.model.HighSchoolDetails;
import com.example.a20190516_parultandel_nycschools.viewmodel.HighSchoolDetailsViewModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class HighScoolDetailsViewModelTest {
    private HighSchoolDetailsViewModel highSchoolDetailsViewModelMock;
    private HighSchoolDetails highSchoolDetailsMock;
    @Mock
    private Context mockApplicationContext;

    @Test
    public void setTestHighSchoolDetailMock(){
        highSchoolDetailsMock=new HighSchoolDetails();

        highSchoolDetailsViewModelMock=new HighSchoolDetailsViewModel(highSchoolDetailsMock,mockApplicationContext);
        assertNotNull(highSchoolDetailsViewModelMock.getSchoolName());
        assertNotNull(highSchoolDetailsViewModelMock.getPhoneNo());
        assertNotNull(highSchoolDetailsViewModelMock.getSchoolAddress());
        assertNotNull(highSchoolDetailsViewModelMock.getWebSite());
    }
}
